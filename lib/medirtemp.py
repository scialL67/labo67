import max31865_smith as smith
import time, math

class max31865(smith.max31865):
    def __init__(self, clkPin=10, misoPin=11, mosiPin=12, csPin=13):
		self.csPin = csPin
		self.misoPin = misoPin
		self.mosiPin = mosiPin
		self.clkPin = clkPin
		self.setupGPIO()

    def getTemp(self):
        #one shot B2
        self.writeRegister(0, 0xA2)

        # conversion time is less than 100ms
        time.sleep(.1) #give it 100ms for conversion

        # read all registers
        out = self.readRegisters(0,8)

        conf_reg = out[0]

        [rtd_msb, rtd_lsb] = [out[1], out[2]]
        rtd_ADC_Code = (( rtd_msb << 8 ) | rtd_lsb ) >> 1

        R_REF = 430.0 # Reference Resistor
        Res0 = 100.0; # Resistance at 0 degC for 400ohm R_Ref
        a = .00390830
        b = -.000000577500
        c = -0.00000000000418301

        Res_RTD = (rtd_ADC_Code * R_REF) / 32768.0 # PT100 Resistance
        temp_C = -(a*Res0) + math.sqrt(a*a*Res0*Res0 - 4*(b*Res0)*(Res0 - Res_RTD))
        temp_C = temp_C / (2*(b*Res0))
        if (temp_C < 0): #use straight line approximation if less than 0
            temp_C = (rtd_ADC_Code/32) - 256

        [hft_msb, hft_lsb] = [out[3], out[4]]
        hft = (( hft_msb << 8 ) | hft_lsb ) >> 1
        [lft_msb, lft_lsb] = [out[5], out[6]]
        lft = (( lft_msb << 8 ) | lft_lsb ) >> 1
        status = out[7]
        if ((status & 0x80) == 1):
           raise FaultError("High threshold limit (Cable fault/open)")
        if ((status & 0x40) == 1):
           raise FaultError("Low threshold limit (Cable fault/short)")
        if ((status & 0x04) == 1):
           raise FaultError("Overvoltage or Undervoltage Error")

        return Res_RTD, temp_C

    def medirTemp(self, med_interval=2, med_duracion=10):
        t0 = time.ticks_ms()
        while time.ticks_diff(time.ticks_ms(), t0)/1000 < med_duracion:
            res, temp = self.getTemp()
            print(f'{time.ticks_diff(time.ticks_ms(), t0)/1000};{res};{temp}')
            time.sleep(med_interval)
