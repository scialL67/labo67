# Labo67

## barra.py
Simulación y visualización de la evolución de una barra 1D con una fuente de potencia controlada por un PID

## PID.py
Implementación de PID

## control_led.py
Script para controlar intensidad y duración del encendido de un LED mediante un PWM

## set_led.py
Script para controlar intensidad de un LED mediante un PWM

## medir_temp.py
Script para medir (y guardar) la temperatura mediante una resistencia PT100 a través de la MAX31865

## /lib
Librerias para medir temperatura con la MAX31865
