import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

class barra1D:
    def __init__(self, dx, dt, L, t, alpha, mu, u, pot, T_amb):
        self.dx = dx
        self.dt = dt
        self.L = L
        self.alpha = alpha
        self.mu = mu
        self.u = u
        self.pot = pot
        self.T_amb = T_amb
        self.t = t

        self._N = int(L/dx)
        self._s = alpha*dt/dx**2
        self._matriz = self._matrizEvo()

    def _matrizEvo(self):
        """ Matriz de evolución del sistema """
        A1 = np.diag([1-2*self._s]*self._N,)
        A2 = np.diag([self._s]*(self._N-1), k=1)
        A3 = np.diag([self._s]*(self._N-1), k=-1)
        return A1+A2+A3

    def _fuenteCte(self):
        """ Condición de contorno para simular una fuente cte de potencia F """
        cont = np.zeros(self._N)
        cont[1] = self.pot
        return cont

    def evolucionar(self, DT):
        """ Evolución iterativa del sistema, durante un intervalo DT, utilizando la matriz de evolución + una fuente cte + emisión térmica """
        tf = self.t + DT # mientras el tiempo actual sea menor al inicial t0 + DT, evolucioná la barra
        while self.t < tf:
            self.t += self.dt # aumento el tiempo actual
            self.u = np.matmul(self._matriz, self.u) + self._fuenteCte()*self.dt - self.mu*(self.u**4 - self.T_amb**4)*self.dt
            self.u[-1] = self.u[-2] # condiciones de contorno libres, de lo contrario tendría temp fija en los bordes
            self.u[0] = self.u[1] # ídem

    def medir(self, x):
        """ Devuelve la temperatura de la barra en la posición x """
        x_i = round((x-self.dx)/self.dx)
        return self.u[x_i]

    def simularLivePrint(self, x_med, DT_med, controlador):
        """ Printea en vivo los valores de temperatura medidos """
        while True:
            self.evolucionar(DT=DT_med)
            T = self.medir(x=x_med)
            controlador.current_time = self.t
            controlador.control(t_medida=T)
            self.pot = controlador.power
            print(f'Temperatura en x={x_med}, t={round(self.t, 3)}: {round(T, 3)}')

    def simularLivePlot(self, x_med, DT_med, controlador):
        """ Plotea en vivo los valores de temperatura medidos """
        temps = []
        tiempo = []
        def animate(i):
            temps.append(self.medir(x=x_med))
            tiempo.append(self.t)
            self.evolucionar(DT=DT_med)
            controlador.current_time = tiempo[-1]
            controlador.control(t_medida=temps[-1])
            self.pot = controlador.power
            plt.cla()
            plt.plot(tiempo, temps, '.-', c='tab:red')
            plt.axhline(y = controlador.t_target, linestyle=':', c='gray', label='Target', linewidth=2)
            plt.xlim([tiempo[-1]-15*DT_med, tiempo[-1]+2*DT_med])
            plt.title(f't={round(tiempo[-1], 2)}', size=15)
            plt.xlabel('Tiempo', size=15)
            plt.ylabel('Temperatura', size=15)
            plt.legend()
        anim = FuncAnimation(plt.gcf(), animate, interval=1)
        plt.show()

    def simularPlot(self, x_med, DT_med, iters, controlador):
        """ Simula todo y luego plotea los valores de temperatura medidos """
        temps = []
        tiempo = []
        i = 0
        while i < iters:
            temps.append(self.medir(x=x_med))
            tiempo.append(self.t)
            self.evolucionar(DT=DT_med)
            controlador.current_time = tiempo[-1]
            controlador.control(t_medida=temps[-1])
            self.pot = controlador.power
            i += 1
        plt.plot(tiempo, temps, '.-', c='tab:red')
        plt.axhline(y = controlador.t_target, linestyle=':', c='gray', label='Target', linewidth=2)
        plt.xlabel('Tiempo', size=15)
        plt.ylabel('Temperatura', size=15)
        plt.legend()
        plt.show()

###
class potControl(object):
    """ Controlador de potencia básico. A reemplazar por el PID """
    def __init__(self, T_target, pot_magnitude):
        self.t_target = T_target
        self.pot_magnitude = pot_magnitude
        self.power = 0

    def control(self, t_medida):
        if t_medida > self.t_target:
            pot = 0
        else:
            pot = self.pot_magnitude
        self.power = pot

if __name__ == '__main__':
    dx = 0.01; dt = dx**2/10; L = 1; t0 = 0 # discretización espacial, temporal, longitud de la barra y tiempo inicial de simulación
    alpha = 1.5; mu = 2 # conductividad térmica y constante de radiación térmica
    N = int(L/dx) # número de segmentos dx
    u0 = np.zeros(N) # condicion inicial de la barra
    pot = 0 # potencia inicial de la fuente
    T_amb = 0 # temperatura ambiente fuera de la barra (de otras cosas que van a emitir hacia la barra)

    barra = barra1D(dx=dx, dt=dt, L=L, t=t0, alpha=alpha, mu=mu, u=u0, pot=pot, T_amb=T_amb) # inicializo el objeto de la barra con sus valores

    x_med = L # punto de medición
    DT_med = 0.05 # tiempo entre mediciones
    T_target = 0.5 # temperatura objetivo
    control_obj = potControl(T_target, pot_magnitude=100)

    barra.simularLivePrint(x_med, DT_med, control_obj)
