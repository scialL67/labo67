#!/usr/bin/python
import sys
import argparse
import serial
import os
from datetime import datetime

SERIAL = "/dev/ttyACM0"

def parse_args():
    parser = argparse.ArgumentParser(description="Medir temperaturas de una PT100 amplificada por una Adafruit MAX 31865. Los datos se guardan en ./temp_out_%d-%m-%Y_%H:%M:%S.")
    parser.add_argument('intervalo', type=float, help="Intervalo de tiempo entre cada medición de temperatura")
    parser.add_argument('duracion', type=float, help="Por cuánto tiempo seguir tomando mediciones")
    return parser.parse_args()

def main():
    args = parse_args()

    ser = serial.Serial(SERIAL,115200)
    print(f'Conectado al puerto serial {SERIAL}')
    commandTemp = f'readTemp,{args.intervalo},{args.duracion}\n'
    out_dir = os.path.dirname(os.path.abspath(__file__)) + f'/{datetime.now().strftime("temp_out_%d-%m-%Y_%H:%M:%S")}'

    try:
        read = True
        ser.write(commandTemp.encode())
        with open(out_dir, 'w') as file:
            while read:
                result = ser.readline().decode().strip()
                if result == "":
                    break
                elif result.startswith('Comando recibido'):
                    print(result)
                    file.write('tiempo(s);resistencia(ohms);temperatura(C)\n')
                else:
                    print(f'Tiempo: {result.split(";")[0]}s. Res.: {result.split(";")[1]}ohms. Temp.: {result.split(";")[2]}C')
                    file.write(result+'\n')

    except KeyboardInterrupt:
        if ser != None:
            print("\nKeyboard interrupt. Cerrando puerto.")
            ser.close()

if __name__=='__main__':
    sys.exit(main())
