#!/usr/bin/python
import sys
import argparse
import serial

SERIAL = "/dev/ttyACM0"

def parse_args():
    parser = argparse.ArgumentParser(description="Controlar iluminación de un LED")
    parser.add_argument('pin', type=int, help="Pin del microcontrolador al cual está conectado el LED")
    parser.add_argument('intensidad', type=int, help="Intensidad del LED como porcentaje de su intensidad máxima (máximo duty cycle en el PWM)")
    parser.add_argument('frecuencia', type=int, help="Frecuencia del PWM que controla el LED")
    return parser.parse_args()

def main():
    args = parse_args()

    ser = serial.Serial(SERIAL, 115200)
    print(f'Conectado al puerto serial {SERIAL}')
    commandLED = f'setLED,{args.pin},{args.intensidad},{args.frecuencia}\n'

    try:
        read = True
        ser.write(commandLED.encode())
        result = "algo"
        while read:
            result = ser.readline().decode().strip()
            if result == "":
                break
            else:
                print(result)

    except KeyboardInterrupt:
        if ser != None:
            print("\nKeyboard Interrupt. Cerrando puerto.")
            ser.close()

    if ser != None:
        print("Fin.")
        ser.close()

if __name__=='__main__':
    sys.exit(main())
