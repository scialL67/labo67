#!/usr/bi/python -tt

from machine import Pin, PWM
import time
import sys
from lib.medirtemp import max31865
from time import sleep

def controlLED(pin=15, intensidad=100, duracion=10, frecuencia=1000):
    pwm = PWM(Pin(pin))
    pwm.freq(frecuencia)
    dutyCycle = int(65025*intensidad/100)
    pwm.duty_u16(dutyCycle)
    sleep(duracion)
    pwm.duty_u16(0)

def setLED(pin=15, intensidad=100, frecuencia=1000):
    pwm = PWM(Pin(pin))
    pwm.freq(frecuencia)
    dutyCycle = int(65025*intensidad/100)
    pwm.duty_u16(dutyCycle)

def readTemp(med_interval=2, med_duracion=12):
    max = max31865()
    max.medirTemp(med_interval, med_duracion)

if __name__ == "__main__":
    commandControlLED = "controlLED"
    commandTemp = "readTemp"
    commandSetLED = "setLED"
    while True:
        command = sys.stdin.readline().strip()
        print(f'Comando recibido {command}')
        if command.startswith(commandControlLED):
            pin = int(command.split(',')[1])
            intensidad = int(command.split(',')[2])
            duracion = float(command.split(',')[3])
            frecuencia = int(command.split(',')[4])
            controlLED(pin, intensidad, duracion, frecuencia)
        elif command.startswith(commandSetLED):
            pin = int(command.split(',')[1])
            intensidad = int(command.split(',')[2])
            frecuencia = int(command.split(',')[3])
            setLED(pin, intensidad, frecuencia)
        elif command.startswith(commandTemp):
            med_interval = float(command.split(',')[1])
            med_duracion = float(command.split(',')[2])
            readTemp(med_interval, med_duracion)
        command = ""
